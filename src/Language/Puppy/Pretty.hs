{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}

module Language.Puppy.Pretty where

import qualified Text.PrettyPrint.Boxes as Box
import qualified Data.Text.Encoding as E
import qualified Data.Map  as M

import Language.Puppy.Prelude
import Language.Puppy.Syntax
import Language.Puppy.PrettyUtils

prettyPrintV :: Value -> Box.Box
prettyPrintV = \case
  Atom n -> bt (":" <> n)
  Int i -> bt $ show i
  Float f -> bt $ show f
  Char c -> bt $ show c
  Text text -> bt ("'''" <> text <> "'''")
  Bytes bs -> bt ("'\"" <> E.decodeUtf8 bs <> "\"'")
  Bool b -> bt $ show b

prettyPrintE :: Expr ann -> Box.Box
prettyPrintE = \case
  Value _ v -> prettyPrintV v
  Symbol _ n -> bt n
  Closure _ _ e -> prettyPrintE e
  Fun _ args body ->
    let pArgs = prettyPrintArgs args
    in bt "(" Box.<> bt "lambda" Box.<+> (pArgs Box.// prettyPrintE body) `endWith` bt ")"
  Quote _ e -> bt "'" Box.<> prettyPrintE e
  List _ es ->
    let pEs = map prettyPrintE es
    in bt "[" Box.<> Box.vcat Box.top pEs `endWith` bt "]"
  App _ es ->
    let pEs = map prettyPrintE es
    in bt "(" Box.<> Box.vcat Box.top pEs `endWith` bt ")"
  Record _ rec ->
    let
      pp (n,e) =
        let pEs = [bt n, prettyPrintE e]
        in bt "[" Box.<> Box.hcat Box.left pEs `endWith` bt "]"
      al = Box.vcat Box.top (map pp $ M.toList rec)
    in
      bt "{" Box.<> al `endWith` bt "}"
      
  If _ cond true false -> 
    let
      c  = prettyPrintE cond
      t  = prettyPrintE true
      f  = prettyPrintE false
      al = Box.vcat Box.top [c,t,f]
    in
      bt "(if" Box.<+> al `endWith` bt ")"

  Let _ lettype l' expr -> 
    let
      t = ppLetType lettype
      l =
        flip map l' $ \(n,e) ->
          bt "[" Box.<> Box.vcat Box.top [bt n, prettyPrintE e] `endWith` bt "]"
    in
      "(" Box.<> t Box.<+> (("[" Box.<+> Box.vcat Box.top l `endWith` bt "]") Box.// prettyPrintE expr) `endWith` ")"

  Do _ stmts ->
    "(" Box.<> "do" Box.<+> Box.vcat Box.top (map prettyPrintStmt stmts) `endWith` ")"

ppLetType :: LetType -> Box.Box
ppLetType = \case
  LetLT -> "let"
  LetStarLT -> "let*"
  LetRecLT -> "letrec"

prettyPrintArgs :: FunArgs ann -> Box.Box
prettyPrintArgs = \case
  FunArgsList _ n -> bt n
  FunArgs _ (map snd -> ns) (map fst -> mlast) ->
    bt "[" Box.<> (Box.vcat Box.top (pEs <> [last])) `endWith` bt "]"
   where
    pEs  = map bt ns
    last = maybe Box.nullBox (\l -> bt "&" Box.<> bt l) mlast

prettyPrintRecDef :: RecordDef ann -> Box.Box
prettyPrintRecDef RecordDef{ exports, defines } =
  let
    exps = prettyPrintArgs $ FunArgs undefined exports Nothing
    defs = map prettyPrintDef defines
  in exps Box.// Box.vcat Box.top defs

prettyPrintStmt :: DoStatement ann -> Box.Box
prettyPrintStmt = \case
  DoExpr e -> prettyPrintE e
  DoDef d -> prettyPrintDef d

prettyPrintDef :: Def ann -> Box.Box
prettyPrintDef = \case
  ValDef _ n e ->
    bt "(val" Box.<+> bt n Box.<+> prettyPrintE e `endWith` bt ")"
  FunDef _ n args e ->
    bt "(fun" Box.<+> bt n Box.<+> (prettyPrintArgs args Box.// prettyPrintE e) `endWith` bt ")"
  RecDef _ n rec ->
    bt "(rec" Box.<+> bt n Box.<+> prettyPrintRecDef rec `endWith` bt ")"

pe :: Expr ann -> IO ()
pe = Box.printBox . prettyPrintE

prettyPrintMod :: Module ann -> Box.Box
prettyPrintMod Module{..} =
    bt "(module" Box.<+> Box.vcat Box.top [bt name, exps, imps, defs] `endWith` bt ")"
  where
    exps = if null exports then bt "" else bt "[" Box.<+> Box.hcat Box.left (fmap bt exports) `endWith` bt "]"
    imps = Box.vcat Box.top (fmap ppImport imports)
    defs = Box.vcat Box.top (fmap prettyPrintDef defines)

ppImport :: ModuleImport -> Box.Box
ppImport ModuleImport{..} =
    bt "(import" Box.<+> bt moduleName Box.<+> imps Box.<> q Box.<> bt ")"
  where
    imps = bt "[" Box.<+> Box.hcat Box.left (fmap bt importNames) Box.<> bt "]"
    q = maybe (bt "") ((bt " " Box.<>) . bt) qualifiedName

