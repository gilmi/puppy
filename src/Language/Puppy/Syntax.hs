{- | Syntax module

The language syntax is designed to be very simple and nimble.
There are modules which contain functions and values declaration

- Scheme-like syntax
- Everything is immutable
- Records built-in
- Atoms/Keywords starts with :
- Erlang style pattern matching

-}

{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}

module Language.Puppy.Syntax where

import Language.Puppy.Prelude

data Annotation = Annotation
  { filename :: FilePath
  , row :: Int
  , column :: Int
  }
  deriving (Show, Read, Eq, Generic)

data Module ann = Module
  { ann     :: ann
  , name    :: Name
  , imports :: [ModuleImport]
  , defines :: [Def ann]
  , exports :: [Name]
  }
  deriving (Show, Read, Eq, Typeable, Generic)

data ModuleImport = ModuleImport
  { moduleName :: Name
  , importNames :: [Name]
  , qualifiedName :: Maybe Name
  }
  deriving (Show, Read, Eq, Typeable, Generic)

data Def ann
  = ValDef ann Name (Expr ann)
  | FunDef ann Name (FunArgs ann) (Expr ann)
  | RecDef ann Name (RecordDef ann)
  deriving (Show, Read, Eq, Typeable, Generic)

data RecordDef ann = RecordDef
  { defines :: [Def ann]
  , exports :: [(ann, Name)]
  }
  deriving (Show, Read, Eq, Typeable, Generic)

data Expr ann
  = Value ann Value
  | Symbol ann Name
  | Closure ann (Env ann) (Expr ann)
  | Fun ann (FunArgs ann) (Expr ann)
  | Quote ann (Expr ann)
  | List ann [Expr ann]
  | App ann [Expr ann]
  | Record ann (Map Name (Expr ann))
  | If ann (Expr ann) (Expr ann) (Expr ann)
  | Let ann LetType [(Name, Expr ann)] (Expr ann)
  | Do ann [DoStatement ann]
  deriving (Show, Read, Eq, Typeable, Generic)

data DoStatement ann
  = DoExpr (Expr ann)
  | DoDef (Def ann)
  deriving (Show, Read, Eq, Typeable, Generic)

data LetType
  = LetLT
  | LetStarLT
  | LetRecLT
  deriving (Show, Read, Eq, Typeable, Generic)

data Env ann
  = Map Name (Expr ann)
  deriving (Show, Read, Eq, Typeable, Generic)

data Value
  = Atom Name
  | Int Int
  | Float Float
  | Char Char
  | Text Text
  | Bytes ByteString
  | Bool Bool
  deriving (Show, Read, Eq, Typeable, Generic)

-- |
-- arguments for a function
-- either it is a list of names arguments can be bind to on by one and a maybe a rest name to bind rest
-- or a name to bind all arguments in a list
data FunArgs ann
  = FunArgs ann [(ann, Name)] (Maybe (Name, ann))
  | FunArgsList ann Name
  deriving (Show, Read, Eq, Typeable, Generic)



class Annotated f where
  getAnn :: f a -> a

instance Annotated FunArgs where
  getAnn = \case
    FunArgs a _ _ -> a
    FunArgsList a _ -> a

instance Annotated Expr where
  getAnn = \case
    Value a _ -> a
    Symbol a _ -> a
    Closure a _ _ -> a
    Fun a _ _ -> a
    Quote a _ -> a
    List a _ -> a
    App a _ -> a
    Record a _ -> a
    If a _ _ _ -> a
    Let a _ _ _ -> a
    Do a _ -> a

instance Annotated Def where
  getAnn = \case
    ValDef a _ _ -> a
    RecDef a _ _ -> a
    FunDef a _ _ _ -> a

instance Annotated Module where
  getAnn = ann

emptyAnn :: Annotation
emptyAnn = Annotation
  { filename = ""
  , row = 0
  , column = 0
  }


ea :: Annotation
ea = emptyAnn
