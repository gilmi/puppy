{-
Welcome to your custom Prelude
Export here everything that should always be in your library scope
For more info on what is exported by Protolude check:
https://github.com/sdiehl/protolude/blob/master/Symbols.md
-}
module Language.Puppy.Prelude
    ( module Exports
    , Name
    ) where

import Protolude as Exports

type Name = Text
