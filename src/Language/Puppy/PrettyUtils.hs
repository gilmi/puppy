
module Language.Puppy.PrettyUtils where

import qualified Text.PrettyPrint.Boxes as Box
import qualified Data.Text as T

import Language.Puppy.Prelude
import Language.Puppy.Syntax

bt :: T.Text -> Box.Box
bt = Box.text . T.unpack

-- | Place a Box on the bottom right of another
endWith :: Box.Box -> Box.Box -> Box.Box
endWith l r = l Box.<> Box.vcat Box.top [Box.emptyBox (Box.rows l - 1) (Box.cols r), r]

li :: [Expr Annotation] -> Expr Annotation
li = List ea
va :: Value -> Expr Annotation
va = Value ea
int :: Int -> Expr Annotation
int = Value ea . Int
la
  :: FunArgs Annotation
     -> Expr Annotation -> Expr Annotation
la xs bd = Fun ea xs bd
ar :: t -> (Annotation, t)
ar = (ea,)

fa :: [Name] -> FunArgs Annotation
fa = flip (FunArgs ea) Nothing . map ar

